const express = require('express');
const router = express.Router();
const mongoose = require('mongoose');
const requireAuth = require('../middlewares/requireAuth')

const db = require ('../db/db')







router.use(requireAuth);














router.get('/flights/:id', async (req, res) =>{


  

    console.log('get flight by id ', req.params)

    
    if( !req.user ){
        res.status(422).send('you must be logged in as admin');
        return;
    }

 
    
    
    try{

        const ret = await db.getFlight(req.params.id)
        res.send(ret);
    }catch (err){
        res.status(422).send(err);
    }
    
    
    

});

router.get('/flights', async (req, res) =>{


    let flight = req.query.flight;

    console.log('get flights', flight)

    
    if( !req.user ){
        res.status(422).send('you must be logged in as admin');
        return;
    }

 
    
    
    try{

        const ret = await db.getFlights(flight)
        res.send(ret);
    }catch (err){
        res.status(422).send(err);
    }
    
    
    

});




router.post('/flights', async (req, res) =>{


    

    const {from, to, departure, landing, price} = req.body;

    console.log('add flight' , from, to, departure, landing, price)

    if( !req.user ){
        res.status(422).send('you must be logged in as admin');
        return;
    }
    
  
    
   
    
    
    try{
        ret = await db.createFlight(from, to, departure, landing, price);
        console.log(ret)
        res.send(ret);
    }catch (err){
        console.log(err)
        res.status(422).send(err);
    }
    
    

});

























module.exports = router;
