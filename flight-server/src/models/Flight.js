const mongoose = require('mongoose');




const flightSchema = new mongoose.Schema({

    from: {
        type: String,
        default: '',
        required: true
    },
    to: {
        type: String,
        default: '',
        required: true
    },
    departure: {
        type: Date,
        default: new Date(),
        required: true
    },
    landing: {
        type: Date,
        default: new Date(),
        required: true
    },
    price: {
        type: Number,
        default: 0,
        required: true
    }
});



mongoose.model('Flight', flightSchema);
