const mongoose = require('mongoose');
const User = mongoose.model('User');
const Flight = mongoose.model('Flight');


module.exports = {
    
    getUser : async (userId)=>{
        const user = await User.findOne({_id: userId});

        
        if (user){
            let ret = {users: {[user._id]: user}}

           
            return ret;
        }
        throw 'not found'
    },


    getFlights : async (flight)=>{


        let flights;

      

            if(!flight || flight.trim().length ==0){

                return {flights: {}, search: {flights:[]}};

            }

            flights = await Flight.find({to: { 
                "$regex": new RegExp(flight.replace(/\s+/g,"\\s+"), "gi")
            }});

      
         
        
        

        
        if (flights){
            let ret = {flights: {},  search: {flights:[]}}

            flights.forEach((flight)=>{
                ret.flights[flight._id] = flight
                ret.search.flights.push(flight._id);
            })

           
            console.log('ret', ret)
           
            return ret;
        }
        throw 'not found'
    },

    getFlight : async (id)=>{

     
    
        let flights = await Flight.find({_id: id})
       
        
        console.log('find flight', flights)

        
        if (flights){

         

 
        

            




            let ret = {flights: {}, search: {flights:[]} }

            flights.forEach((flight)=>{
                ret.flights[flight._id] = flight
                ret.search.flights.push(flight._id);
            })

          
            console.log('ret', ret)
           
            return ret;
        }
        throw 'not found'
    },

    createFlight : async (from, to, departure, landing, price)=>{

        console.log('create flight')
    

      


      
        const flight = new Flight({ from: from, to:to, departure: departure, landing: landing, price: price});
        await flight.save();
        return {flights: {[flight._id]:flight}}

    
    },
    
 



 
    
    

}
