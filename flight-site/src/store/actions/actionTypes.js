export const ACTION_TEST = 'test';
export const ACTION_LOGIN = 'login';
export const ACTION_SIGN_OUT = 'signout';
export const ACTION_SET_ERROR_MESSAGE = 'set_error_message';
export const ACTION_CLEAR_ERROR =  'clear_error';
export const ACTION_STARTED =  'action_started';

export const ACTION_INIT =  'init';
export const ACTION_CLEAR_SEARCH_FLIGHTS =  'clear_search_flights';
export const ACTION_SET_SEARCH_TERM =  'set_search_term';
export const ACTION_SET_DATA =  'set_data';
