import * as actionTypes from './actionTypes'

import flightsApi from '../../api/flights'


const inProgressApiCalls = {}

let timer = null;

const apiCall = async (method, api, params, dispatch) => {
    const callKey = api + '_' + JSON.stringify(params);
    try{

       
        if (inProgressApiCalls[callKey]){
            return;
        }
        inProgressApiCalls[callKey] = true;
        console.log('params', params)
        let response = null;
        params.withCredentials = true
        switch(method){
            case 'get':
                response = await flightsApi.get('/'+ api, params);
                break;
            case 'post':
                response = await flightsApi.post('/'+ api, params);
                break;
            case 'put':
                response = await flightsApi.put('/'+ api, params);
                break;
            case 'delete':
                response = await flightsApi.delete('/'+ api, params);
                break;
        }
        inProgressApiCalls[callKey] = false;
        console.log(response.data);
        dispatch ({
            type: actionTypes.ACTION_SET_DATA,
            data: response.data
        });
             
            
    }catch(error){
        inProgressApiCalls[callKey] = false;
        console.log(error);
            
    }
}




export const getFlight = (flightId)=>{

    return (dispatch, getState)=>{
        apiCall('get','flights/' +flightId  ,{}, dispatch);
    }

   
        
}





export const searchFlights =  (flight)=>{
    return (dispatch, getState)=>{
        dispatch ({
            type: actionTypes.ACTION_SET_SEARCH_TERM,
            value: flight?flight:''
        });
        dispatch ({
            type: actionTypes.ACTION_CLEAR_SEARCH_FLIGHTS
           
        });


        if( timer){
            clearTimeout(timer)
        }
        timer = setTimeout(()=>{
            apiCall('get','flights' +(flight && flight.trim().length > 0 ?'?flight=' +flight:'') ,{}, dispatch);
        }, 1000)
            
     
    } 
}


export const addFlight = (flight)=>{

    return (dispatch, getState)=>{

        apiCall('post','flights' ,{...flight}, dispatch);
    }

   
        
}


export const init =  ()=>{
    return (dispatch, getState)=>{
        dispatch ({
            type: actionTypes.ACTION_INIT,
        });

      
    } 
}




















