


import { search } from 'superagent';
import * as actionTypes from '../actions/actionTypes'


const initialState = {  search: {}, searchTerm: '', flights:{}}


const mainReducer = (state = initialState, action) => {
    switch(action.type){

        case actionTypes.ACTION_INIT:
            return initialState;

        case actionTypes.ACTION_CLEAR_SEARCH_FLIGHTS:
            return {...state, search: {...state.search, flights:[]}}
        

        case actionTypes.ACTION_SET_SEARCH_TERM:
            return {...state, searchTerm: action.value}
        

        case actionTypes.ACTION_SET_DATA:


            const newState = {...state};
            const keys = Object.keys(action.data);
            for (let i = 0 ; i < keys.length; i++){
                const entityName = keys[i]

                const entity = {...state[entityName]};
                const ids = Object.keys(action.data[entityName]);
                for (let j = 0 ; j < ids.length; j++){
                    const id = ids[j];
                    entity[id] = action.data[entityName][id]
                }
                newState[entityName] = {...entity}
            }
            return newState;
        
        default:
            return state;
    }
}

export default mainReducer;
