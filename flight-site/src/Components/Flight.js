import React from 'react'
import {NavLink} from 'react-router-dom'
import classes from './Flight.css'
import logo from '../assets/images/logo.png'
import { faEdit, faCartPlus, faMinusCircle } from '@fortawesome/free-solid-svg-icons'

import Moment from 'moment'

import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'


class Flight extends React.Component{
    render(){

        return (<div className={classes.FlightDetails}>
                
                <div className={classes.Vertical}>
                    <h1 className= {classes.Details}   >from: {this.props.flight.from}</h1>
                    <h1 className={classes.Details}>to: {this.props.flight.to}</h1>
                    <h1 className={classes.Details}>depatrure: {Moment(this.props.flight.departure).format('DD/MM/YYYY')}</h1>
                    <h1 className={classes.Details}>landing: {Moment(this.props.flight.landing).format('DD/MM/YYYY')}</h1>
  
                    
                </div>

            </div>)
    }
}

export default Flight;