import React, { Component } from 'react';
import Backdrop from './Backdrop';
import Aux from './AuxComponent';

import classes from './Modal.css'

const modal = (props)=>{

    /*shouldComponentUpdate(nextProps,  nextState){
        return nextProps.show !== this.props.show || nextProps.children !== this.props.children;
    }*/


    return ( 
        <Aux>
            <Backdrop show = {props.show} clicked = {props.backdropClickedHandler}/>
            <div onClick  = {props.backdropClickedHandler} className = {classes.ModalContainer} style = {{
                    transform: props.show ? 'translateY(0)':'translateY(-100vh)',
                    opacity: props.show ? '1': '0'
                }}>
                <div  onClick = {
                    (e)=> {
                        e.stopPropagation();
                    
                        console.log('handleChildClick');
                }} className = {classes.Modal}
                >
                    {props.children}
                </div>
            </div>

        </Aux>
    )
    
}



export default React.memo(modal, (prevProps, nextProps)=> {
    return nextProps.show === prevProps.show && nextProps.children === prevProps.children;
});