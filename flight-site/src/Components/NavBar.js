import React from 'react'
import {NavLink, withRouter} from 'react-router-dom'
import classes from './NavBar.css'
import logo from '../assets/images/logo.png'
class NavBar extends React.Component{
    render(){
        return <header className={classes.Toolbar}>
            <div className={classes.Logo}>
                <img src = {logo}></img>
            </div>
            
            <nav className={classes.Nav}>

                <ul className={classes.NavigationItems}>

                
                    <li className = {classes.NavigationItem}>
                        <NavLink  activeClassName = {classes.active} exact to="/">Flights</NavLink>
                    </li>
                    <li className = {classes.NavigationItem}>
                        <NavLink activeClassName = {classes.active} exact to="/add">Add</NavLink>
                    </li>

             

                </ul>
            </nav>
            <NavLink   onClick= {()=>{this.props.onSignOut()}} className = {classes.Signout} exact to="/">Sign Out</NavLink>
            
        </header>
    }
}

export default withRouter(NavBar) ;