import React from 'react';
import {connect} from 'react-redux'
import * as authActionCreators from '../store/actions/authActions'
import loading from '../assets/images/loading.gif'
import classes from './LoginScreen.css'
import arrow from '../assets/images/arrow.png'
import {NavLink} from 'react-router-dom'


class LoginScreen extends React.Component{

    constructor(){
        super();
        this.state = {
            inputsValid:false,
            username: '',
            password: ''
        }

    }


    componentDidMount(){

    }




    render(){

        console.log('login screen state', this.state)

        if (this.props.inProgress){
            return (
                <div className = {classes.ActivityIndicator} >
                    <img src = {loading}/>
                </div>
            )
        }

        const checkUsername = (username)=>{
  
            return username.length !== 0;
        }

        const checkPassword = (password)=>{
            return password.length !== 0;
        }

        const checkValidity = ()=>{
            const email = document.getElementById("username").value ;
            const password = document.getElementById("password").value ;
            if( checkUsername(email)== false){
                return false;
            }
            if( checkPassword(password) == false){
                return false;
            }
            return true;
        }

    

        const submitForm = ()=>{
            console.log("submit!!!!!!!!!!!!!!!")
           // const email = document.getElementById("username").value ;
           // const password = document.getElementById("password").value ;

            this.props.onSignIn({
                username:this.state.username, 
                password:this.state.password
            })
            return true;
        }



        return (<div className = {classes.Main}>
                <h1>Log In</h1>
                <form className = {classes.Form} onSubmit = {
                    (e)=>{
                        submitForm();
                        return true;
                    }
                }>
                
                    <input value = {this.state.username} type="text" onChange = {
                        (event)=>{

                            this.setState({
                                ...this.state,
                                username: event.target.value,
                                inputsValid:checkValidity(),
                                
                            })
                        }
                    } id="username" name="fname" placeholder = "user name"/>
                    <input value = {this.state.password} onChange = {
                        (event)=>{
                            this.setState({
                                ...this.state,
                                password: event.target.value,
                                inputsValid:checkValidity(),
                                
                            })
                        }
                    } type="password" id="password" name="lname" placeholder = "Password"/>
                    {
                        (this.props.errorMessage && this.props.errorMessage.length !== 0)?
                        <h1 className = {classes.Error}>{this.props.errorMessage}</h1>: null

                    }
                    
                   
                   
                    <button disabled = {this.state.inputsValid == false? true: false} className={classes.Submit} > <img  src= {arrow} /></button>
                </form>
                <NavLink exact to="/signup">Don't have an account? Sign up now!</NavLink>
                
        </div>)
    }
}

const mapStateToProps = (state)=>{
    console.log('state from props loading', state)
    return {
        user: state.auth.user,
        inProgress: state.auth.inProgress,
        errorMessage: state.auth.errorMessage
    }
}

const mapDispatchToProps = (dispatch)=>{
    return {
        onSignIn: (data) => {dispatch(authActionCreators.signin(data))}
    }
}

export default connect(mapStateToProps, mapDispatchToProps)( LoginScreen);