import React from 'react';
import {connect} from 'react-redux'
import * as authActionCreators from '../store/actions/authActions'
import loading from '../assets/images/loading.gif'
import classes from './SignupScreen.css'
import arrow from '../assets/images/arrow.png'
import {NavLink} from 'react-router-dom'




class SignupScreen extends React.Component{

    constructor(){
        super();
        this.state = {
            inputsValid:false,
            password: '',
            username: ''
        }

    }


    componentDidMount(){

    }




    render(){

        console.log('login screen state', this.state)

        if (this.props.inProgress){
            return (
                <div className = {classes.ActivityIndicator} >
                    <img src = {loading}/>
                </div>
            )
        }



        const checkPassword = (password)=>{
            return password.length !== 0;
        }





        const checkUsername = (username)=>{
            if (username.length ==0){
                return false;
            }
            const pattern = /^[a-zA-Z0-9]*$/;
            const res =  pattern.test(username);
            console.log('res', username, res)
            return res;
        }

        const checkValidity = ()=>{
            const password = document.getElementById("password").value ;
            const username = document.getElementById("username").value ;
   
            if(checkPassword(password) == false){
                return false;
            }
   
            if(checkUsername(username) == false){
                return false;
            }
            return true;
        }

    

        const submitForm = ()=>{
            console.log("submit!!!!!!!!!!!!!!!")
            const username = document.getElementById("username").value ;
            const password = document.getElementById("password").value ;
            console.log(username, password)
            this.props.onSignUp({
                password,
                username
              })
            return true;
        }



        return (<div className = {classes.Main}>
                <h1>Sign Up</h1>
                <form className = {classes.Form} onSubmit = {
                    (e)=>{
                        submitForm();
                        return true;
                    }
                }>
                
                    

                    <input value = {this.state.username} type="text" onChange = {
                        (event)=>{

                            if (checkUsername(event.target.value) == false && event.target.value.length != 0){
                                return;
                            }

                            this.setState({
                                ...this.state,
                                username: event.target.value,
                                inputsValid:checkValidity(),
                                
                            })
                        }
                    } id="username" name="fname" placeholder = "User Name"/>

                   

                  

                    <input value = {this.state.password} onChange = {
                        (event)=>{
                            this.setState({
                                ...this.state,
                                password: event.target.value,
                                inputsValid:checkValidity(),
                                
                            })
                        }
                    } type="password" id="password" name="lname" placeholder = "Password"/>
                    {
                        (this.props.errorMessage && this.props.errorMessage.length !== 0)?
                        <h1 className = {classes.Error}>{this.props.errorMessage}</h1>: null

                    }
                    
                   
                   
                    <button disabled = {this.state.inputsValid == false? true: false} className={classes.Submit} > <img  src= {arrow} /></button>
                </form>
                <NavLink  exact to="/">Already have an account? Sign In!</NavLink>
        </div>)
    }
}

const mapStateToProps = (state)=>{
    console.log('state from props loading', state)
    return {
        user: state.auth.user,
        inProgress: state.auth.inProgress,
        errorMessage: state.auth.errorMessage
    }
}

const mapDispatchToProps = (dispatch)=>{
    return {
        onSignUp: (data) => {dispatch(authActionCreators.signup(data))}
    }
}

export default connect(mapStateToProps, mapDispatchToProps)( SignupScreen);