import React  from 'react'
import classes from './FlightsScreen.css'
import {connect} from 'react-redux'
import withMainStore from '../hooks/withMainStore'
import Flight from '../Components/Flight'
import { withRouter } from 'react-router'

class FlightsScreen extends React.Component{

    constructor(){
        super()
    }

    componentDidMount(){
        this.props.searchFlights()
    }
    render(){

        const flightIds = this.props.getFlights()
        console.log('flight ids', flightIds);

        return <div className = {classes.Main}>
            <input value = {this.props.searchTerm} type="text" onChange = {

                        (event)=>{


                            this.props.searchFlights(event.target.value)
               

                        }
                    
                    } id="term" name="fname" placeholder = "Please enter a flight destination"/>
                <ul>
                {flightIds.length == 0 ? 
                 'No flights found. Please type another destination': flightIds.map( (id)=>{
                    const flight = this.props.getFlight(id)

                    return(
                    <li key = {flight._id}>
                        <Flight  flight = {flight} 
                        
                        ></Flight>
                    </li>)
                })}
                </ul>

        </div>
    }
}





const mapStateToProps = (state)=>{
    console.log('mapStateToProps', this.props)
    return {
        authUser: state.auth.user,
        inProgress: state.auth.inProgress,
        errorMessage: state.auth.errorMessage
    }
}

const mapDispatchToProps = (dispatch)=>{
    return {
        
    }
}


export default  connect(mapStateToProps, mapDispatchToProps)( withRouter(withMainStore(FlightsScreen)) );