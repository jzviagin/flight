import React from 'react';
import {connect} from 'react-redux'
import * as authActionCreators from '../store/actions/authActions'
import loading from '../assets/images/loading.gif'
import classes from './AddFlightScreen.css'
import arrow from '../assets/images/arrow.png'
import {NavLink, withRouter} from 'react-router-dom'
import withMainStore from '../hooks/withMainStore';




class AddFlightScreen extends React.Component{

    constructor(){
        super();
        this.state = {
            inputsValid:false,
            from: '',
            to: '',
            price: '',
  
           
        }

    }


    componentDidMount(){
     
       
    }

    shouldComponentUpdate(nextProps, nextState){



        return true;
    }

    submitForm (){

        const from = document.getElementById("from").value ;
        const to = document.getElementById("to").value ;
        const price = document.getElementById("price").value ;

        console.log("submit!!!!!!!!!!!!!!!",this.props.onSubmit)
        console.log("submit!!!!!!!!!!!!!!!",{
     
            from: this.state.from,
            to: this.state.to,
            price: this.state.price,
            departure: new Date(),
            landing: new Date()
        })
        this.props.onSubmit({
            from: this.state.from,
            to: this.state.to,
            price: +this.state.price,
            departure: new Date(),
            landing: new Date()
        })

        this.props.history.push('/' )
    

     
        return true;
    }

    




    render(){



        

       

       
      




        const checkName = (name)=>{
            if (name.length ==0){
                return false;
            }
            const pattern = /^[a-zA-Z0-9]*$/;
            const res =  pattern.test(name);
            console.log('res', name, res)
            return res;
        }

        const checkNum = (num)=>{
            if (num.length ==0){
                return false;
            }
            const pattern = /^[0-9]*$/;
            const res =  pattern.test(num);
            console.log('res', num, res)
            return res;
        }

        const checkValidity = ()=>{
           
            const from = document.getElementById("from").value ;

            const to = document.getElementById("to").value ;

            const price = document.getElementById("price").value ;


   
   
            if(checkName(from) == false){
                return false;
            }

            if(checkName(to) == false){
                return false;
            }

            if(checkNum(price) == false){
                return false;
            }
            return true;
        }

    

        



        return (<div className = {classes.Main}>
                <h1>Add Flight</h1>
                <form className = {classes.Form} onSubmit = {
                    (e)=>{
                        this.submitForm.call(this);
                        e.preventDefault()
                        return true;
                    }
                }>
                
                    
                    
                    <input value = {this.state.from} type="text" onChange = {
                        (event)=>{

                            if (checkName(event.target.value) == false && event.target.value.length != 0){
                                return;
                            }

                            this.setState({
                                ...this.state,
                                from: event.target.value,
                                inputsValid:checkValidity(),
                                
                            })
                        }
                    } id="from" name="ffrom" placeholder = "From"/>

                    <input value = {this.state.to} type="text" onChange = {
                        (event)=>{

                            if (checkName(event.target.value) == false && event.target.value.length != 0){
                                return;
                            }

                            this.setState({
                                ...this.state,
                                to: event.target.value,
                                inputsValid:checkValidity(),
                                
                            })
                        }
                    } id="to" name="fto" placeholder = "To"/>

                    
                    <input value = {this.state.price} type="text" onChange = {
                                            (event)=>{



                                                if (checkNum(event.target.value) == false && event.target.value.length != 0){
                                                    return;
                                                }

                                                this.setState({
                                                    ...this.state,
                                                    price: event.target.value,
                                                    inputsValid:checkValidity(),
                                                    
                                                })
                                            }
                                        } id="price" name="fprice" placeholder = "Price"/>

                    
                      
                   
                    <button disabled = {this.state.inputsValid == false? true: false} className={classes.Submit} > <img  src= {arrow} /></button>
                </form>
                
        </div>)
    }
}


export default  withMainStore( withRouter(AddFlightScreen) );