
import React from 'react';
import {connect} from 'react-redux'
import * as mainActionCreators from '../store/actions/mainActions'


const withMainStore = (Child)=>{
    const MainComp = (props)=>{

        console.log('maincomp', props)
        const funcs = {

            getFlight :  (flightId)=>{
                if (props['flights'][flightId]){
                    return props['flights'][flightId]
                }
                props.dispatch(mainActionCreators.getFlight(flightId));
                return null;
            
    
            
                    
            },
    
            fetchFlight : (flightId)=>{
    
                props.dispatch(mainActionCreators.getFlight(flightId));
                return null;     
            },

    
            getFlights : ()=>{
    
                
                if (props['search']['flights']){
                    return props['search']['flights']
                }
                return [];
            },
    
    
    
    
            searchFlights : (flight)=>{
                props.dispatch(mainActionCreators.searchFlights(flight))
                return [];
            },

            addFlight :  (flight)=>{
            
                props.dispatch(mainActionCreators.addFlight(flight));
                return null;
            
    
            
                    
            },

 

    
    
         
    
          

        }


        const newProps = {...props, ...funcs}

      

        return <Child {...newProps}></Child>
    }

    const mapStateToProps = (state)=>{
        return { 
            search: state.main.search, 
            searchTerm: state.main.searchTerm, 
            flights:state.main.flights,
 
           
        }
    }

    const mapDispatchToProps = (dispatch)=>{
        return {
            dispatch: dispatch
        }
    }

    return connect(mapStateToProps, mapDispatchToProps)(MainComp)
}

export default withMainStore;